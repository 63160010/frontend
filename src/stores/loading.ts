import { ref } from "vue";
import { defineStore } from "pinia";

export const useLodingStore = defineStore("loding", () => {
  const isLoding = ref(false);

  return { isLoding };
});
